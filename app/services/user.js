const Joi = require("joi");
// const { ObjectId } = require("mongodb");
const UserModel = require("../model/user");
// const DbService = require("./db.services");

class UserService {
  validateUser = (data) => {
    try {
      let userSchema = Joi.object({
        name: Joi.string().min(3).required(),
        email: Joi.string().email().required(),
        password: Joi.string().min(8).required(),
        address: Joi.string(),
        role: Joi.string().required(),
        status: Joi.string().required(),
        image: Joi.string().empty(),
      });
      let response = userSchema.validate(data);
      //   console.log(response);
      if (response.error) {
        throw response.error.details[0].message;
      }
    } catch (err) {
      console.log(err);
      throw err;
    }
  };

  createUser = async (data) => {
    try {
      // return await this.db.collection("users").insertOne(data); 
      // OR
      let user_obj =new UserModel(data);
      // console.log("user.servisce ", user_obj)
      return await user_obj.save();

      // OR
      // return await UserModel.insert(data);
    } catch (excep) {
      if(excep.code === 11000){
        let keys=Object.keys(excep.keyPattern);
        throw keys.join(", ") + " should be unique";
      }
      else{
        throw excep;
      }
    }
  };

  getUserByEmail = async (data) => {
    try {
      // let result = await this.db.collection("users").findOne({
      //   email: data.email,
      //   // password: data.password,
      // });
        let result = await UserModel.findOne({
        email: data.email,
      });
      return result;
    } catch (error) {
      console.log(error);
      throw error;
    }
  };

  getUserById = async (id) => {
    try {
      // let user = this.db.collection("users").findOne({
      //   _id: ObjectId(id),
      // });
      let user = await UserModel.findById(id);
      return user;
    } catch (excep) {
      throw excep;
    }
  };
}

module.exports = UserService;
