const Joi = require("joi");
const { created_by } = require("../model/common.Schema");
const ProductModel = require("../model/product");

class ProductService {
  storeValidate = (data, created_by) => {
    try {
      let schema = Joi.object({
        name: Joi.string().required().min(2),
            slug: Joi.string(),
            images: Joi.array().items(Joi.string().allow(null).allow('')).default(null),
            description: Joi.string().allow(null,''),
            price: Joi.number().required().min(1),
            actual_price: Joi.number().required().min(1),
            discount: Joi.number().allow(null, '').min(0).max(100),
            brand: Joi.string().allow(null, '').default(null),
            seller: Joi.string().allow(null, ''),
            is_featured: Joi.boolean().default(false),
            status: Joi.string().valid("active",'inactive').default('inactive')
      });
      let response = schema.validate(data);
      if (response.error) {
        throw response.error.details[0].message;
      } else {
        this.data = data;
        this.data.created_by = created_by;
      }
    } catch (excep) {
      console.log("her", excep);
      throw excep;
    }
  };

  createProduct = async () => {
    let product_obj = new ProductModel(this.data);
    return await product_obj.save();
  };

  getAllCount = async () => {
    let all_data = await ProductModel.find();
    return all_data.length;
  };

  getProducts = async (skip, limit) => {
    return await ProductModel.find()
      .skip(skip)
      .limit(limit);
  };

  findById = async (id) => {
    return await ProductModel.findById(id)
  };

  deleteById = async (id) => {
    return await ProductModel.findByIdAndRemove(id);
  };

  updateProduct = async (id) => {
    let status = await ProductModel.findByIdAndUpdate(id, {
      $set: this.data,
    });
    return status;
  };
}

module.exports = ProductService;
