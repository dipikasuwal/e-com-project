const mongoose = require("mongoose");
// const commonSchema =require("../model/common.Schema")

const UserSchemaDef = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      enum: ["admin", "customer", "seller"],
      default: "customer",
    },
    status: {
      type: String,
      enum: ["active", "inactive"],
      default: "inactive",
    },
    //   address: {
    //     shipping: AddressSchemanDef,
    //     billing: AddressSchemanDef
    //   },
    address: {
      type: String,
      default: null,
    },
    image: {
      type: String,
    },
    created_by: {
      type: mongoose.Types.ObjectId,
      ref: "User",
      default: null,
    },
  },
  {
    autoCreate: true,
    autoIndex: true,
    timestamps: true,
  }
);

const UserModel = mongoose.model("User", UserSchemaDef);
module.exports = UserModel;
