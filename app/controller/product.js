const ProductService = require("../services/product");
const slugify = require("slugify");

class ProductController {
  constructor() {
    this.product_svc = new ProductService();
  }

  productStore = async (req, res, next) => {
    try {
      let data = req.body;
      data.images = [];
      if (req.files) {
        // console.log(req.file)
        req.files.map((item) => {
          data.images.push(item.filename);
        });
      }

      data.slug = slugify(data.name, {
        lower: true,
      });

      if (!data.brand || data.brand == "null") {
        data.brand = null;
      }

      if (!data.seller || data.seller == "null") {
        data.seller = null;
      }

    //   if (!data.category_id || data.category_id == "null") {
    //     data.category_id = null;
    //   } else {
    //     data.category_id = data.category_id.split(",");
    //   }

      data.actual_price = Number(data.price) - Number(data.price) * Number(data.discount) / 100;

      //   data.is_featured=data.is_feature ? true :false;
      // or
      data.is_featured= !!data.is_featured; //!!true => true and !!1 => true

      this.product_svc.storeValidate(data, req.auth_user_id);
      let response = await this.product_svc.createProduct();
      res.json({
        result: response,
        msg: "Product created successfully",
        status: true,
      });
    } catch (excep) {
      console.log("Product Store: ", excep);
      next({ status: 400, msg: excep });
    }
  };

  getProducts = async (req, res, next) => {
    try {
      let paginate = {
        total_count: await this.product_svc.getAllCount(),
        per_page: req.query.per_page ? parseInt(req.query.per_page) : 10,
        current_page: req.query.page ? parseInt(req.query.page) : 1,
      };
      let skip = (paginate.current_page - 1) * paginate.per_page;
      let data = await this.product_svc.getProducts(skip, paginate.per_page);
      res.json({
        result: data,
        status: true,
        paginate: paginate,
        msg: "Data fetched",
      });
    } catch (excep) {
      console.log("List Categories: ", excep);
      next({ status: 400, msg: excep });
    }
  };

  getProductById = async (req, res, next) => {
    try {
      let data = await this.product_svc.findById(req.params.id);
      if (data) {
        res.json({
          result: data,
          status: true,
          msg: "Data fetched by Id",
        });
      } else {
        next({ status: 404, msg: "Data doesnot exists" });
      }
    } catch (excep) {
      console.log("Fetched Product By Id: ", excep);
      next({ status: 400, msg: excep });
    }
  };

  deleteById = async (req, res, next) => {
    try {
      let data = await this.product_svc.deleteById(req.params.id);
      if (data) {
        res.json({
          result: data,
          status: true,
          msg: "Deleted by Id",
        });
      } else {
        next({ status: 404, msg: "Data doesnot exists" });
      }
    } catch (excep) {
      console.log("Fetched Product By Id: ", excep);
      next({ status: 400, msg: excep });
    }
  };

  productUpdate = async (req, res, next) => {
    try {
      let current_data = await this.product_svc.findById(req.params.id);
      let data = req.body;
      data.images=current_data.images;
      if (req.files) {
        // console.log(req.file)
        req.files.map((item) => {
            data.images.push(item.filename);
        })
      } 

      data.slug = current_data.slug;

      if (!data.brand || data.brand == "null") {
        data.brand = null;
      }

      if (!data.seller || data.seller == "null") {
        data.seller = null;
      }

      data.actual_price = Number(data.price) - Number(data.price) * Number(data.discount) / 100;

      //   data.is_featured=data.is_feature ? true :false;
      // or
      data.is_featured= !!data.is_featured; 

      this.product_svc.storeValidate(data);
      let response = await this.product_svc.updateProduct(req.params.id);
      res.json({
        result: response,
        msg: "Product updated successfully",
        status: true,
      });
    } catch (excep) {
      console.log("Update Product: ", excep);
      next({ status: 400, msg: excep });
    }
  };
}

module.exports = ProductController;
