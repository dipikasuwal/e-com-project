const UserService = require("../services/user");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const Config = require("../../config/config");
// const { MongoClient } = require("mongodb");

class AuthController {
  constructor() {
    this.user_svc = new UserService();
  }

  registerUser = async (req, res, next) => {
    try {
      let body = req.body;
      if (req.file) {
        body.image = req.file.filename;
      }
      this.user_svc.validateUser(body);
      body.password = bcrypt.hashSync(body.password, 10);
      let data = await this.user_svc.createUser(body);

      res.json({
        result: data,
        status: true,
        msg: "Register Data Test",
      });
    } catch (excep) {
      next({ status: 400, msg: excep });
    }
  };

  loginUser = async (req, res, next) => {
    try {
      let body = req.body;
      let loggedInUser = await this.user_svc.getUserByEmail(body);

      if (loggedInUser) {
        if (bcrypt.compareSync(body.password, loggedInUser.password)) {
          let token = jwt.sign(
            {
              user_id: loggedInUser._id,
            },
            Config.JWT_SECRET
          );
          res.json({
            result: {
              user: loggedInUser,
              access_token: token,
            },
            status: true,
            msg: "Login Success",
          });
        } else {
          next({ status: 400, msg: "Password does not match" });
        }
      } else {
        next({ status: 400, msg: "Crenditials does not match" });
      }
    } catch (error) {
      next({ status: "400", msg: error });
    }
  };

  logout = async (req, res, next) => {
    try {
    } catch (error) {
      throw error;
    }
  };

  getLoggedInUser =(req, res, next) => {
    res.json({
      result: req.auth_user,
      status: true, 
      msg:"Your Profile"
    })
  }
}

module.exports = AuthController;
