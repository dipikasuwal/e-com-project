const auth = require("./auth");
var product = require("./product");

var express = require("express");
var app = express();

app.use(auth);
app.use("/product", product);

module.exports = app;
