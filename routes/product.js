const auth = require("../app/middleware/auth");
const { isAdmin } = require("../app/middleware/rbac");
const uploader = require("../app/middleware/uploader");

const ProductController = require("../app/controller/product");
const product_ctrl = new ProductController();

const router = require("express").Router();

router.route("/")
    .get(product_ctrl.getProducts)
    .post(auth, isAdmin, uploader.array('images'), product_ctrl.productStore)

router.route("/:id")
    .get(product_ctrl.getProductById)
    .delete(auth, isAdmin, product_ctrl.deleteById)
    .put(auth, isAdmin,uploader.array("images"), product_ctrl.productUpdate)
module.exports = router;