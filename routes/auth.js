const app_routes = require("express").Router();
const AuthController = require("../app/controller/auth");
const auth_ctrl = new AuthController();
const auth= require("../app/middleware/auth")

const uploader=require("../app/middleware/uploader");

app_routes.post("/login",auth_ctrl.loginUser);

app_routes.post("/register", uploader.single("image"), auth_ctrl.registerUser);

app_routes.post("/logout",auth, auth_ctrl.logout);

app_routes.post("/change-pwd", (req, res) => {});

app_routes.get("/test", auth, auth_ctrl.getLoggedInUser)

module.exports = app_routes;
